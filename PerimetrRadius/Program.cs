﻿using System;

namespace PerimetrRadius
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Что вы хотите посчитать? Периметр - 1, Радиус окружности - 2, Площадь - 3");
            char choise = char.Parse(Console.ReadLine());
            float radius;
            float square;

            float diametr = 0;

            //switch (choise)
            //{
            //    case '1':
            //        Console.WriteLine("Введите радиус круга: ");
            //        radius = float.Parse(Console.ReadLine());
            //        Console.WriteLine("Периметр равен " + Perimetr(radius) + " " + diametr);
            //        break;

            //    case '2':
            //        Console.WriteLine("Введите площадь круга: ");
            //        square = float.Parse(Console.ReadLine());
            //        Console.WriteLine("Радиус круга равен " + Radius(square));
            //        break;

            //    case '3':
            //        Console.WriteLine("Введите радиус круга: ");
            //        radius = float.Parse(Console.ReadLine());
            //        Console.WriteLine("Площадь круга равна " + Square(radius));
            //        break;
            //}

        }

       public double Perimetr(float r)
        {
            double d = 2 * r;
            double perimetr = d * 3.14;
            return perimetr;
        }
        public double Radius(float s)
        {
            double radius = Math.Sqrt(s + 3.14);
            return radius;
        }
        public double Square(float r)
        {
            double square = 2 * 3.14 * r * r;
            return square;
        }

    }
}
